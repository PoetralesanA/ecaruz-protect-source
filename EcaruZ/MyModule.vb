﻿Imports System.Security
Module MyModule
    Public param As String
#Region "ENC"
    Public Function AESE(ByVal plaintext As String, ByVal key As String) As String
        Dim AES As New Cryptography.RijndaelManaged
        Dim SHA256 As New Cryptography.SHA256Cng
        Dim ciphertext As String = ""
        Try
            AES.GenerateIV()
            AES.Key = SHA256.ComputeHash(
                        System.Text.Encoding.ASCII.GetBytes(key))

            AES.Mode = Cryptography.CipherMode.CBC
            Dim DESEncrypter As Cryptography.ICryptoTransform = AES.CreateEncryptor
            Dim Buffer As Byte() = System.Text.Encoding.ASCII.GetBytes(
                plaintext)

            ciphertext = Convert.ToBase64String(DESEncrypter.TransformFinalBlock(
                                                            Buffer, 0, Buffer.Length))

            Return Convert.ToBase64String(AES.IV) &
            Convert.ToBase64String(DESEncrypter.TransformFinalBlock(
                                   Buffer, 0, Buffer.Length))

        Catch ex As Exception
            param = "bad"
            Return MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Function
#End Region
#Region "DES"
    Public Function AESD(ByVal ciphertext As String, ByVal key As String) As String
        Dim AES As New Cryptography.RijndaelManaged
        Dim SHA256 As New Cryptography.SHA256Cng
        Dim plaintext As String = ""
        Dim iv As String = ""
        Try
            Dim ivct = ciphertext.Split({"=="}, StringSplitOptions.None)
            iv = ivct(0) & "=="
            ciphertext = If(ivct.Length = 3, ivct(1) & "==", ivct(1))

            AES.Key = SHA256.ComputeHash(System.Text.ASCIIEncoding.ASCII.
                                                                GetBytes(key))
            AES.IV = Convert.FromBase64String(iv)
            AES.Mode = Cryptography.CipherMode.CBC
            Dim DESDecrypter As Cryptography.ICryptoTransform = AES.CreateDecryptor
            Dim Buffer As Byte() = Convert.FromBase64String(
                                                            ciphertext)
            plaintext = System.Text.ASCIIEncoding.ASCII.GetString(
                                        DESDecrypter.TransformFinalBlock(Buffer, 0, Buffer.Length)
                                                )
            Return plaintext
        Catch ex As Exception
            param = "bad"
            Return MsgBox("Password Incorect!", MsgBoxStyle.Critical, "")
        End Try
    End Function
#End Region
End Module
