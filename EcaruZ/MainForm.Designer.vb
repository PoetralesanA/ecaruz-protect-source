﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.Tab_Enc = New System.Windows.Forms.TabPage()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.label_line = New System.Windows.Forms.Label()
        Me.label_total = New System.Windows.Forms.Label()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txt_confirmkey = New System.Windows.Forms.TextBox()
        Me.txt_key = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Btn_Encrypt = New System.Windows.Forms.Button()
        Me.btn_browseFile = New System.Windows.Forms.Button()
        Me.txt_path = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Tab_DeS = New System.Windows.Forms.TabPage()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.LabelDesLine = New System.Windows.Forms.Label()
        Me.LabelDesTotal = New System.Windows.Forms.Label()
        Me.btn_browseFile_ = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txt_confirmkey_ = New System.Windows.Forms.TextBox()
        Me.txt_key_ = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Btn_Des = New System.Windows.Forms.Button()
        Me.txt_path_ = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.MyProgressbar = New System.Windows.Forms.ProgressBar()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.MyVersion = New System.Windows.Forms.Label()
        Me.OPFD_MyFile = New System.Windows.Forms.OpenFileDialog()
        Me.BgwEncrypt = New System.ComponentModel.BackgroundWorker()
        Me.BgwDES = New System.ComponentModel.BackgroundWorker()
        Me.TabControl1.SuspendLayout()
        Me.Tab_Enc.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.Tab_DeS.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.Tab_Enc)
        Me.TabControl1.Controls.Add(Me.Tab_DeS)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Font = New System.Drawing.Font("Century", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(-1, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(380, 258)
        Me.TabControl1.TabIndex = 0
        '
        'Tab_Enc
        '
        Me.Tab_Enc.Controls.Add(Me.Panel1)
        Me.Tab_Enc.Controls.Add(Me.label_line)
        Me.Tab_Enc.Controls.Add(Me.label_total)
        Me.Tab_Enc.Controls.Add(Me.ProgressBar1)
        Me.Tab_Enc.Controls.Add(Me.GroupBox1)
        Me.Tab_Enc.Controls.Add(Me.btn_browseFile)
        Me.Tab_Enc.Controls.Add(Me.txt_path)
        Me.Tab_Enc.Controls.Add(Me.Label1)
        Me.Tab_Enc.Location = New System.Drawing.Point(4, 25)
        Me.Tab_Enc.Name = "Tab_Enc"
        Me.Tab_Enc.Padding = New System.Windows.Forms.Padding(3)
        Me.Tab_Enc.Size = New System.Drawing.Size(372, 229)
        Me.Tab_Enc.TabIndex = 0
        Me.Tab_Enc.Text = "Encrypt"
        Me.Tab_Enc.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Gainsboro
        Me.Panel1.Location = New System.Drawing.Point(8, 191)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(358, 1)
        Me.Panel1.TabIndex = 11
        '
        'label_line
        '
        Me.label_line.AutoSize = True
        Me.label_line.Font = New System.Drawing.Font("Century", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_line.ForeColor = System.Drawing.Color.BlueViolet
        Me.label_line.Location = New System.Drawing.Point(9, 210)
        Me.label_line.Name = "label_line"
        Me.label_line.Size = New System.Drawing.Size(80, 15)
        Me.label_line.TabIndex = 10
        Me.label_line.Text = "Line Encrypt :"
        '
        'label_total
        '
        Me.label_total.AutoSize = True
        Me.label_total.Font = New System.Drawing.Font("Century", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_total.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.label_total.Location = New System.Drawing.Point(9, 195)
        Me.label_total.Name = "label_total"
        Me.label_total.Size = New System.Drawing.Size(61, 15)
        Me.label_total.TabIndex = 9
        Me.label_total.Text = "Total File :"
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(8, 176)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(356, 10)
        Me.ProgressBar1.TabIndex = 8
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txt_confirmkey)
        Me.GroupBox1.Controls.Add(Me.txt_key)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Btn_Encrypt)
        Me.GroupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.GroupBox1.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.GroupBox1.Location = New System.Drawing.Point(3, 55)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(360, 115)
        Me.GroupBox1.TabIndex = 6
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Key"
        '
        'txt_confirmkey
        '
        Me.txt_confirmkey.Enabled = False
        Me.txt_confirmkey.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.txt_confirmkey.Location = New System.Drawing.Point(128, 48)
        Me.txt_confirmkey.Name = "txt_confirmkey"
        Me.txt_confirmkey.Size = New System.Drawing.Size(217, 22)
        Me.txt_confirmkey.TabIndex = 7
        Me.txt_confirmkey.TabStop = False
        '
        'txt_key
        '
        Me.txt_key.Enabled = False
        Me.txt_key.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.txt_key.Location = New System.Drawing.Point(128, 20)
        Me.txt_key.Name = "txt_key"
        Me.txt_key.Size = New System.Drawing.Size(217, 22)
        Me.txt_key.TabIndex = 6
        Me.txt_key.TabStop = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label3.Location = New System.Drawing.Point(15, 48)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(86, 15)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Confirm Key    :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label2.Location = New System.Drawing.Point(15, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(87, 15)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Key                   :"
        '
        'Btn_Encrypt
        '
        Me.Btn_Encrypt.Enabled = False
        Me.Btn_Encrypt.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Btn_Encrypt.Location = New System.Drawing.Point(128, 76)
        Me.Btn_Encrypt.Name = "Btn_Encrypt"
        Me.Btn_Encrypt.Size = New System.Drawing.Size(217, 32)
        Me.Btn_Encrypt.TabIndex = 0
        Me.Btn_Encrypt.Text = "Encrypt"
        Me.Btn_Encrypt.UseVisualStyleBackColor = True
        '
        'btn_browseFile
        '
        Me.btn_browseFile.BackgroundImage = Global.EcaruZ.My.Resources.Resources.icons8_browse_folder_80
        Me.btn_browseFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btn_browseFile.FlatAppearance.BorderSize = 0
        Me.btn_browseFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_browseFile.Location = New System.Drawing.Point(341, 25)
        Me.btn_browseFile.Name = "btn_browseFile"
        Me.btn_browseFile.Size = New System.Drawing.Size(24, 22)
        Me.btn_browseFile.TabIndex = 3
        Me.btn_browseFile.UseVisualStyleBackColor = True
        '
        'txt_path
        '
        Me.txt_path.BackColor = System.Drawing.Color.White
        Me.txt_path.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_path.Font = New System.Drawing.Font("Century", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_path.Location = New System.Drawing.Point(3, 27)
        Me.txt_path.MaxLength = 0
        Me.txt_path.Name = "txt_path"
        Me.txt_path.ReadOnly = True
        Me.txt_path.Size = New System.Drawing.Size(336, 21)
        Me.txt_path.TabIndex = 2
        Me.txt_path.Text = "Browse Your File!"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label1.Location = New System.Drawing.Point(3, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(103, 16)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Project to Encrypt"
        '
        'Tab_DeS
        '
        Me.Tab_DeS.Controls.Add(Me.Panel2)
        Me.Tab_DeS.Controls.Add(Me.LabelDesLine)
        Me.Tab_DeS.Controls.Add(Me.LabelDesTotal)
        Me.Tab_DeS.Controls.Add(Me.btn_browseFile_)
        Me.Tab_DeS.Controls.Add(Me.GroupBox2)
        Me.Tab_DeS.Controls.Add(Me.txt_path_)
        Me.Tab_DeS.Controls.Add(Me.Label6)
        Me.Tab_DeS.Controls.Add(Me.MyProgressbar)
        Me.Tab_DeS.Location = New System.Drawing.Point(4, 25)
        Me.Tab_DeS.Name = "Tab_DeS"
        Me.Tab_DeS.Padding = New System.Windows.Forms.Padding(3)
        Me.Tab_DeS.Size = New System.Drawing.Size(372, 229)
        Me.Tab_DeS.TabIndex = 1
        Me.Tab_DeS.Text = "Decrypt"
        Me.Tab_DeS.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Gainsboro
        Me.Panel2.Location = New System.Drawing.Point(8, 191)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(358, 1)
        Me.Panel2.TabIndex = 16
        '
        'LabelDesLine
        '
        Me.LabelDesLine.AutoSize = True
        Me.LabelDesLine.Font = New System.Drawing.Font("Century", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelDesLine.ForeColor = System.Drawing.Color.BlueViolet
        Me.LabelDesLine.Location = New System.Drawing.Point(9, 210)
        Me.LabelDesLine.Name = "LabelDesLine"
        Me.LabelDesLine.Size = New System.Drawing.Size(79, 15)
        Me.LabelDesLine.TabIndex = 15
        Me.LabelDesLine.Text = "Line Decrypt :"
        '
        'LabelDesTotal
        '
        Me.LabelDesTotal.AutoSize = True
        Me.LabelDesTotal.Font = New System.Drawing.Font("Century", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelDesTotal.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.LabelDesTotal.Location = New System.Drawing.Point(9, 195)
        Me.LabelDesTotal.Name = "LabelDesTotal"
        Me.LabelDesTotal.Size = New System.Drawing.Size(61, 15)
        Me.LabelDesTotal.TabIndex = 14
        Me.LabelDesTotal.Text = "Total File :"
        '
        'btn_browseFile_
        '
        Me.btn_browseFile_.BackgroundImage = Global.EcaruZ.My.Resources.Resources.icons8_browse_folder_80
        Me.btn_browseFile_.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btn_browseFile_.FlatAppearance.BorderSize = 0
        Me.btn_browseFile_.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_browseFile_.Location = New System.Drawing.Point(343, 26)
        Me.btn_browseFile_.Name = "btn_browseFile_"
        Me.btn_browseFile_.Size = New System.Drawing.Size(24, 22)
        Me.btn_browseFile_.TabIndex = 13
        Me.btn_browseFile_.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txt_confirmkey_)
        Me.GroupBox2.Controls.Add(Me.txt_key_)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.Btn_Des)
        Me.GroupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.GroupBox2.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.GroupBox2.Location = New System.Drawing.Point(6, 55)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(360, 115)
        Me.GroupBox2.TabIndex = 11
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Key"
        '
        'txt_confirmkey_
        '
        Me.txt_confirmkey_.Enabled = False
        Me.txt_confirmkey_.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.txt_confirmkey_.Location = New System.Drawing.Point(128, 48)
        Me.txt_confirmkey_.Name = "txt_confirmkey_"
        Me.txt_confirmkey_.Size = New System.Drawing.Size(217, 22)
        Me.txt_confirmkey_.TabIndex = 7
        Me.txt_confirmkey_.TabStop = False
        '
        'txt_key_
        '
        Me.txt_key_.Enabled = False
        Me.txt_key_.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.txt_key_.Location = New System.Drawing.Point(128, 20)
        Me.txt_key_.Name = "txt_key_"
        Me.txt_key_.Size = New System.Drawing.Size(217, 22)
        Me.txt_key_.TabIndex = 6
        Me.txt_key_.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Label4.Font = New System.Drawing.Font("Century Gothic", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label4.Location = New System.Drawing.Point(15, 48)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(89, 15)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Confirm Key     :"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Label5.Font = New System.Drawing.Font("Century Gothic", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label5.Location = New System.Drawing.Point(15, 24)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(88, 15)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Your Key          :"
        '
        'Btn_Des
        '
        Me.Btn_Des.Enabled = False
        Me.Btn_Des.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Btn_Des.Location = New System.Drawing.Point(128, 76)
        Me.Btn_Des.Name = "Btn_Des"
        Me.Btn_Des.Size = New System.Drawing.Size(217, 32)
        Me.Btn_Des.TabIndex = 0
        Me.Btn_Des.Text = "Decryption"
        Me.Btn_Des.UseVisualStyleBackColor = True
        '
        'txt_path_
        '
        Me.txt_path_.BackColor = System.Drawing.Color.White
        Me.txt_path_.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_path_.Font = New System.Drawing.Font("Century", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_path_.Location = New System.Drawing.Point(6, 27)
        Me.txt_path_.MaxLength = 0
        Me.txt_path_.Name = "txt_path_"
        Me.txt_path_.ReadOnly = True
        Me.txt_path_.Size = New System.Drawing.Size(336, 21)
        Me.txt_path_.TabIndex = 10
        Me.txt_path_.Text = "Browse Your File!"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Century Gothic", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label6.Location = New System.Drawing.Point(6, 8)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(105, 16)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "Project to Decrypt"
        '
        'MyProgressbar
        '
        Me.MyProgressbar.Location = New System.Drawing.Point(8, 176)
        Me.MyProgressbar.Name = "MyProgressbar"
        Me.MyProgressbar.Size = New System.Drawing.Size(356, 10)
        Me.MyProgressbar.TabIndex = 12
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.Label8)
        Me.TabPage3.Controls.Add(Me.LinkLabel1)
        Me.TabPage3.Controls.Add(Me.PictureBox3)
        Me.TabPage3.Controls.Add(Me.Label7)
        Me.TabPage3.Controls.Add(Me.PictureBox2)
        Me.TabPage3.Controls.Add(Me.Panel3)
        Me.TabPage3.Controls.Add(Me.PictureBox1)
        Me.TabPage3.Controls.Add(Me.MyVersion)
        Me.TabPage3.Location = New System.Drawing.Point(4, 25)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(372, 229)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "About"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Century", 6.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.Label8.Location = New System.Drawing.Point(147, 188)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(106, 13)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "Donate / Buy Source?"
        '
        'LinkLabel1
        '
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.Font = New System.Drawing.Font("Century", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel1.Location = New System.Drawing.Point(232, 204)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(76, 15)
        Me.LinkLabel1.TabIndex = 6
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "PoetralesanA"
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = Global.EcaruZ.My.Resources.Resources.fb
        Me.PictureBox3.Location = New System.Drawing.Point(207, 203)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(19, 19)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox3.TabIndex = 5
        Me.PictureBox3.TabStop = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Century", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(90, 203)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(92, 15)
        Me.Label7.TabIndex = 4
        Me.Label7.Text = "+xxxxxxxxxxxxx"
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.EcaruZ.My.Resources.Resources.wa
        Me.PictureBox2.Location = New System.Drawing.Point(65, 202)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(19, 19)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 3
        Me.PictureBox2.TabStop = False
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.DarkGray
        Me.Panel3.Location = New System.Drawing.Point(48, 184)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(276, 1)
        Me.Panel3.TabIndex = 2
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(150, -7)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(73, 74)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'MyVersion
        '
        Me.MyVersion.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.MyVersion.Location = New System.Drawing.Point(-4, 65)
        Me.MyVersion.Name = "MyVersion"
        Me.MyVersion.Size = New System.Drawing.Size(380, 120)
        Me.MyVersion.TabIndex = 1
        Me.MyVersion.Text = "Version 1.0.0.0" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Created By PoetralesanA" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Thanks For My Friends :" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "BlackdemoN"
        Me.MyVersion.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'OPFD_MyFile
        '
        '
        'BgwEncrypt
        '
        Me.BgwEncrypt.WorkerSupportsCancellation = True
        '
        'BgwDES
        '
        Me.BgwDES.WorkerSupportsCancellation = True
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(378, 256)
        Me.Controls.Add(Me.TabControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "MainForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.TopMost = True
        Me.TabControl1.ResumeLayout(False)
        Me.Tab_Enc.ResumeLayout(False)
        Me.Tab_Enc.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.Tab_DeS.ResumeLayout(False)
        Me.Tab_DeS.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents Tab_Enc As System.Windows.Forms.TabPage
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Btn_Encrypt As System.Windows.Forms.Button
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents txt_path As System.Windows.Forms.TextBox
    Friend WithEvents btn_browseFile As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txt_confirmkey As System.Windows.Forms.TextBox
    Friend WithEvents txt_key As System.Windows.Forms.TextBox
    Friend WithEvents OPFD_MyFile As System.Windows.Forms.OpenFileDialog
    Friend WithEvents BgwEncrypt As System.ComponentModel.BackgroundWorker
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents BgwDES As System.ComponentModel.BackgroundWorker
    Friend WithEvents Tab_DeS As System.Windows.Forms.TabPage
    Friend WithEvents btn_browseFile_ As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txt_confirmkey_ As System.Windows.Forms.TextBox
    Friend WithEvents txt_key_ As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Btn_Des As System.Windows.Forms.Button
    Friend WithEvents txt_path_ As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents MyProgressbar As System.Windows.Forms.ProgressBar
    Friend WithEvents label_line As System.Windows.Forms.Label
    Friend WithEvents label_total As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents LabelDesLine As System.Windows.Forms.Label
    Friend WithEvents LabelDesTotal As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents MyVersion As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents Label8 As System.Windows.Forms.Label

End Class
