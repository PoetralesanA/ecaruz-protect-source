﻿Imports System.IO
Public Class MainForm
    Private WithEvents TimerRunBGW_Enc, TimerRunBGW_Des As New Timer
    Private WithEvents OutputFile As New TextBox
    Private WithEvents ListFile As New ListBox
    Private spasi = vbNewLine
    Private CalculateFile, CalculateLine As Integer
    Private ManipulationFileCal, ManipCalculateLine As String
    '//Information 
    '1. Cek Calculation All File 
    '2. Cek Calculation All Line
#Region "Encry"
    Private Sub MyLoad() Handles MyBase.Load
        TimerRunBGW_Des.Interval = 500
        TimerRunBGW_Enc.Interval = 500
        CheckForIllegalCrossThreadCalls = False
        Me.Text = "EcaruZ | version " &
            Application.ProductVersion '( Name & My Version Software )

        MyVersion.Text = My.Resources.msGx.ToString()

        Label7.Text = My.Resources._wa
    End Sub
    Private Sub TXTClickBrowse(sender As Object, e As EventArgs) Handles txt_path.Click
        If txt_path.Text = "Browse Your File!" Then
            MsgBox("Silahkan Cari File Anda Yang Ingin Di Proteksi!",
                   MsgBoxStyle.Exclamation, "")
        End If
    End Sub

    Private Sub ButtonClickEnrypt(sender As Object, e As EventArgs) Handles Btn_Encrypt.Click
        Btn_Encrypt.Enabled = False
        If txt_key.Text = "" Or txt_confirmkey.Text = "" Then ' Cek Pwd Kosong
            MessageBox.Show(
                           "Key not be empty!", "",
                           MessageBoxButtons.OK, MessageBoxIcon.Exclamation
                               )
            Btn_Encrypt.Enabled = True
        Else
            If txt_key.Text <> txt_confirmkey.Text Then ' Cek Pwd
                MessageBox.Show(
                                "Your Password Not Same!", "",
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation
                                    )
                Btn_Encrypt.Enabled = True
            Else
                '// Lakukan Prosses
                If ListFile.Items.Count = 0 Then
                    MsgBox("List File Not Found!", MsgBoxStyle.Exclamation, "")
                    Btn_Encrypt.Enabled = True
                Else
                    TimerRunBGW_Enc.Start()
                End If
            End If
        End If
    End Sub
    Private Sub ProssesFunction()
        Btn_Encrypt.Enabled = True
        txt_confirmkey.Enabled = True
        txt_key.Enabled = True
    End Sub
    Private Sub ProssesFunctionDisable()
        OutputFile.Clear()
        txt_key.Clear()
        txt_confirmkey.Clear()
        ListFile.Items.Clear()
        txt_key.Enabled = False
        Btn_Encrypt.Enabled = False
        txt_confirmkey.Enabled = False
        txt_path.Text = "Browse Your File!"
    End Sub

    Private Sub BgwEncrypt_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles BgwEncrypt.DoWork
        If ListFile.Items.Count <> 0 Then

            If ListFile.SelectedIndex = ListFile.Items.Count - 1 Then
                TimerRunBGW_Enc.Stop()
                BgwEncrypt.CancelAsync()
                ProssesFunctionDisable()
                ManipulationFileCal = CalculateFile
                ManipCalculateLine = CalculateLine
                MessageBox.Show(ManipulationFileCal & " File Found" & spasi &
                                "Total Line Encrypt = " & ManipCalculateLine, "Encryption Done~", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                'order list
                ListFile.SelectedIndex += 1
                CalculateFile += 1
                ProgressBar1.Value = 0
                'Read File > Select Output Text
                Dim Baca = File.ReadAllLines(ListFile.SelectedItem)
                For Each BacaList In Baca
                    If BacaList.Contains("'EcaruZ") Then
                        TimerRunBGW_Enc.Stop()
                        MsgBox("Failed to encrypt" & spasi &
                               "Because file has been encrypted/Not from EcaruZ!", MsgBoxStyle.Critical, "Encrypt Failed!")
                        ProssesFunctionDisable()
                        Exit Sub
                    Else
                        OutputFile.Text += "'EcaruZ" & AESE(BacaList, txt_key.Text) & spasi
                        If param <> "bad" Then
                            '---------------- Tulis File ----------------'
                            Dim _Write As New StreamWriter(ListFile.SelectedItem.ToString)
                            For Each _Tulis In OutputFile.Text
                                _Write.Write(_Tulis)
                            Next
                            _Write.Close()
                            CalculateLine += 1

                            label_total.Text = "Total File : " & CalculateFile
                            label_line.Text = "Line Encrypt : " & CalculateLine
                        ElseIf param = "bad" Then
                            param = String.Empty
                            ProssesFunctionDisable()

                            TimerRunBGW_Enc.Stop()
                            Exit Sub
                        End If
                    End If
                Next
            End If
        End If
    End Sub
    Private Sub EndProssesEncrypt() Handles BgwEncrypt.RunWorkerCompleted
        ' Deleted List Output After Encrypt
        ProgressBar1.Value = 100
        OutputFile.Clear()
    End Sub
    Private Sub RunTimerEN() Handles TimerRunBGW_Enc.Tick
        Try
            BgwEncrypt.RunWorkerAsync()
        Catch ex As Exception
            BgwEncrypt.CancelAsync()
        End Try
    End Sub

    Public Sub _BrowseFile(sender As Object, e As EventArgs) Handles btn_browseFile.Click
        CalculateFile = 0
        OPFD_MyFile.Filter = "Search File vbproj |*.vbproj"
        OPFD_MyFile.ShowDialog()
    End Sub
    Private Sub _OkBrowseFile() Handles OPFD_MyFile.FileOk
        If TabControl1.SelectedTab Is Tab_DeS Then
            '//PROSSES DES
            ProssesFunction_()
            txt_path_.Text = Path.GetDirectoryName(OPFD_MyFile.FileName)

            Try
                'Get All File Directory
                Dim FileArray As String() = Directory.GetFiles(
                                                                Path.GetFullPath(txt_path_.Text),
                                                               "*.vb", SearchOption.AllDirectories)
                For Each GetAllExtension In FileArray
                    ListFile.Items.Add(GetAllExtension)
                Next
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Exclamation, "")
            End Try
            MyProgressbar.Value = 0
        ElseIf TabControl1.SelectedTab Is Tab_Enc Then
            '//PROSSES ENC
            ProssesFunction()
            txt_path.Text = Path.GetDirectoryName(OPFD_MyFile.FileName)
            Try
                'Get All File Directory
                Dim FileArray As String() = Directory.GetFiles(
                                                                Path.GetFullPath(txt_path.Text),
                                                               "*.vb", SearchOption.AllDirectories)
                For Each GetAllExtension In FileArray
                    ListFile.Items.Add(GetAllExtension)
                Next
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Exclamation, "")
            End Try
            ProgressBar1.Value = 0
        End If
    End Sub
#End Region
#Region "DES~"
    '----------------------------------------------------------- DES -----------------------------------------------------------'

    Private Sub btn_browseFile_Click(sender As Object, e As EventArgs) Handles btn_browseFile_.Click
        CalculateFile = 0
        CalculateLine = 0
        OPFD_MyFile.Filter = "Search File vbproj |*.vbproj"
        OPFD_MyFile.ShowDialog()
    End Sub

    Private Sub Btn_Des_Click(sender As Object, e As EventArgs) Handles Btn_Des.Click
        Btn_Des.Enabled = False
        If txt_key_.Text = "" Or txt_confirmkey_.Text = "" Then ' Cek Pwd Kosong
            MessageBox.Show(
                           "Key not be empty!", "",
                           MessageBoxButtons.OK, MessageBoxIcon.Exclamation
                               )
            Btn_Des.Enabled = True
        Else
            If txt_key_.Text <> txt_confirmkey_.Text Then ' Cek Pwd
                MessageBox.Show(
                                "Your Password Not Same!", "",
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation
                                    )
                Btn_Encrypt.Enabled = True
            Else
                '// Lakukan Prosses
                If ListFile.Items.Count = 0 Then
                    MsgBox("List File Not Found!", MsgBoxStyle.Exclamation, "")
                    Btn_Des.Enabled = True
                Else
                    TimerRunBGW_Des.Start()
                End If
            End If
        End If
    End Sub
    Private Sub ProssesFunction_()
        Btn_Des.Enabled = True
        txt_confirmkey_.Enabled = True
        txt_key_.Enabled = True
    End Sub
    Private Sub ProssesFunctionDisableDES()
        OutputFile.Clear()
        txt_key_.Clear()
        txt_confirmkey_.Clear()
        ListFile.Items.Clear()
        txt_key_.Enabled = False
        Btn_Des.Enabled = False
        txt_confirmkey_.Enabled = False
        txt_path_.Text = "Browse Your File!"
    End Sub

    Private Sub BgwDES_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles BgwDES.DoWork
        If ListFile.Items.Count <> 0 Then
            If ListFile.SelectedIndex = ListFile.Items.Count - 1 Then
                TimerRunBGW_Des.Stop()
                BgwDES.CancelAsync()
                ProssesFunctionDisableDES()
                ManipulationFileCal = CalculateFile
                ManipCalculateLine = CalculateLine
                MessageBox.Show(ManipulationFileCal & " File Found" & spasi &
                                "Total Line " & ManipCalculateLine & " Completed in Decrypt", "Decrypt Done~", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                'order list
                ListFile.SelectedIndex += 1
                CalculateFile += 1
                MyProgressbar.Value = 0
                'Read File > Select Output Text
                Dim Baca = File.ReadAllLines(ListFile.SelectedItem)
                For Each BacaList In Baca
                    If Not BacaList.Contains("'EcaruZ") Then
                        TimerRunBGW_Des.Stop()
                        MsgBox("Failed to DES" & spasi &
                               "Because file not Encrypt.", MsgBoxStyle.Critical, "DES Failed!")
                        ProssesFunctionDisableDES()
                        Exit Sub
                    Else
                        OutputFile.Text += AESD(BacaList.Replace("'EcaruZ", ""), txt_key_.Text) & spasi
                        If param <> "bad" Then
                            '---------------- Write File ----------------'
                            Dim _Write As New StreamWriter(ListFile.SelectedItem.ToString)
                            For Each _Tulis In OutputFile.Text
                                _Write.Write(_Tulis)
                            Next
                            _Write.Close()
                            CalculateLine += 1

                            LabelDesTotal.Text = "Total File : " & CalculateFile
                            LabelDesLine.Text = "Line Decrypt : " & CalculateLine
                        Else
                            param = String.Empty
                            ProssesFunctionDisableDES()
                            TimerRunBGW_Des.Stop()
                            Exit Sub
                        End If
                    End If
                Next
            End If
        End If
    End Sub
    Private Sub BgwDES_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BgwDES.RunWorkerCompleted
        ' Deleted List Output After Encrypt
        MyProgressbar.Value = 100
        OutputFile.Clear()
    End Sub
    Private Sub RunTimerDES() Handles TimerRunBGW_Des.Tick
        Try
            BgwDES.RunWorkerAsync()
        Catch ex As Exception
            BgwDES.CancelAsync()
        End Try
    End Sub
#End Region

    Private Sub TabControl1_MouseClick(sender As Object, e As MouseEventArgs) Handles TabControl1.MouseClick
        '// Prosses Reset Pgb in Tab
        If TabControl1.SelectedTab Is Tab_DeS Then
            ProgressBar1.Value = 0

            label_total.Text = "Total File :"
            label_line.Text = "Line Encrypt :"
        Else
            If TabControl1.SelectedTab Is Tab_Enc Then
                MyProgressbar.Value = 0

                LabelDesTotal.Text = "Total File :"
                LabelDesLine.Text = "Line Decrypt :"
            End If
        End If
        '///////////////////////////////
    End Sub

    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        Dim MyMSG = MessageBox.Show("Visited Link?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
        If MyMSG = Windows.Forms.DialogResult.Yes Then
            Process.Start("https://www.facebook.com/PoetralesanA")
        End If
    End Sub

    Private Sub txt_path__Click(sender As Object, e As EventArgs) Handles txt_path_.Click
        If txt_path_.Text = "Browse Your File!" Then
            MsgBox("Silahkan Cari File Anda Yang Ingin Di Decrypt!",
                   MsgBoxStyle.Exclamation, "")
        End If
    End Sub
End Class